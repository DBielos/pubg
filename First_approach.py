# -*- coding: utf-8 -*-
"""
Created on Sat Oct  6 17:42:12 2018

@author: DANIEL MARTINEZ

TODO:
    * Gradien boost model
    * KNN model
    * ANN with adam optimizer, squared error loss function and relu activation
    * Include / Exclude outliers
    * Include team and match features
    * SHAP feature importance
"""
# Import libraries
import numpy as np
import pandas as pd 
import matplotlib.pyplot as plt

#Create Dataframe
df = pd.read_csv('train.csv')

# Transform data for teams.
df_groups = (df.groupby('groupId', as_index=False).agg({'Id':'count', 'matchId':'mean', 'assists':'sum', 'boosts':'sum',
                                'damageDealt':'sum', 'DBNOs':'sum', 'headshotKills':'sum',
                                'heals':'sum', 'killPlace':'mean', 'killPoints':'max', 'kills':'sum',
                                'killStreaks':'mean', 'longestKill':'mean', 'maxPlace':'mean', 'numGroups':'mean',
                                'revives':'sum', 'rideDistance':'max', 'roadKills':'sum', 'swimDistance':'max',
                                'teamKills':'sum', 'vehicleDestroys':'sum', 'walkDistance':'max',
                                'weaponsAcquired':'sum','winPoints':'max', 'winPlacePerc':'mean'}).rename(columns={'Id':'teamSize'}).reset_index())

#----------------------------
#Get aggregation by match
df_matches = (df_groups.groupby('matchId', as_index=False).agg({'teamSize':'sum', 'assists':'mean', 'boosts':'mean',
                                'damageDealt':'sum', 'DBNOs':'mean', 'headshotKills':'sum',
                                'heals':'mean', 'kills':'sum',
                                'killStreaks':'mean', 'longestKill':'max', 'numGroups':'mean',
                                'revives':'mean', 'rideDistance':'mean', 'roadKills':'mean', 'swimDistance':'mean',
                                'teamKills':'sum', 'vehicleDestroys':'sum', 'walkDistance':'mean',
                                'weaponsAcquired':'sum'}).rename(columns={'teamSize':'matchSize'}).reset_index())

X = df_matches.iloc[:,0:21].values    
from sklearn.decomposition import PCA
pca = PCA(n_components=2)
X = pca.fit_transform(X)

fig = plt.figure()
ax = fig.add_subplot(1,1,1)
ax.scatter(X[:,0], X[:,1])
ax.set(xlabel='PC1', ylabel='PC2')
plt.show()

from sklearn.cluster import KMeans
from scipy.spatial.distance import cdist
# k means determine k
X = df_matches.iloc[:,0:21].values
distortions = []
K = range(1,10)
for k in K:
    kmeanModel = KMeans(n_clusters=k).fit(X)
    kmeanModel.fit(X)
    distortions.append(sum(np.min(cdist(X, kmeanModel.cluster_centers_, 'euclidean'), axis=1)) / X.shape[0])

# Plot the elbow
plt.plot(K, distortions, 'bx-')
plt.xlabel('k')
plt.ylabel('Distortion')
plt.grid(True)
plt.title('The Elbow Method showing the optimal k')
plt.show()
#------------------------------

solos = df[df['teamSize'] == 1]
fig = plt.figure()
ax = fig.add_subplot(1,1,1)
ax.scatter(solos['heals'], solos['winPlacePerc'])
ax.set(title='Win Place Percentile by Heals Used', xlabel='Heals Used', ylabel='Win Place Percentile')
plt.show()

df['totalDistance'] = df.apply(lambda dataset: dataset['swimDistance']+dataset['walkDistance']+dataset['rideDistance'], axis='columns')
# Total Distance (all)
fig = plt.figure()
ax = fig.add_subplot(1,1,1)
ax.scatter(df_test['totalDistance'], df_test['winPlacePerc'])
ax.set(title='Win Place Percentile by Total Distance (Swim + Walk + Ride)', xlabel='Total Distance (m)', ylabel='Win Place Percentile')
plt.show()

# Walk Distance (all)
fig = plt.figure()
ax = fig.add_subplot(1,1,1)
ax.scatter(df['assists'], df['winPlacePerc'])
plt.show()

# Total Distance (team)
fig = plt.figure()
ax = fig.add_subplot(1,1,1)
ax.scatter(df['totalDistance'], df['winPlacePerc'])
ax.set(title='Win Place Percentile by Total Distance (Swim + Walk + Ride)', xlabel='Total Distance (m)', ylabel='Win Place Percentile')
plt.show()

# Walk Distance (team)
fig = plt.figure()
ax = fig.add_subplot(1,1,1)
ax.scatter(df['walkDistance'], df['winPlacePerc'])
ax.set(title='Win Place Percentile by Walk Distance', xlabel='Walk Distance (m)', ylabel='Win Place Percentile')
plt.show()

winners = df[df['winPlacePerc'] >= 0.9]
fig = plt.figure()
ax = fig.add_subplot(1,1,1)
ax.bar(winners['assists'].unique(), 463108, color='r', align='center')
ax.bar(df[df['winPlacePerc'] < 0.9]['assists'].unique(), 3894228,color='b', align='center')
ax.set(title='Assists of top 10% (red) vs others (blue)')
plt.show()

fig = plt.figure()
ax = fig.add_subplot(1,1,1)
ax.scatter(range(0,df_matches['kills'].count()),df_matches['kills'])
plt.show()